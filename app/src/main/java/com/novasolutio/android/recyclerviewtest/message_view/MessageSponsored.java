package com.novasolutio.android.recyclerviewtest.message_view;

public class MessageSponsored implements MessageRow {

    private int id;
    private String mLink;
    private String mLinkId;



    public MessageSponsored(int id, String link, String linkId) {
        this.id = id;
        this.mLink = link;
        this.mLinkId = linkId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        mLink = link;
    }

    public String getLinkId() {
        return mLinkId;
    }

    public void setLinkId(String linkId) {
        mLinkId = linkId;
    }

    @Override
    public int getMessageType() {
        return MessageRow.MESSAGE_SPONSORED;
    }
}

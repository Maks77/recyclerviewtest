package com.novasolutio.android.recyclerviewtest.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.novasolutio.android.recyclerviewtest.R;
import com.novasolutio.android.recyclerviewtest.adapters.ListAdapter;
import com.novasolutio.android.recyclerviewtest.message_view.MessageRow;
import com.novasolutio.android.recyclerviewtest.presenters.MainActivityPresenter;
import com.novasolutio.android.recyclerviewtest.views.MainActivityView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MainActivityView, ListAdapter.OnMessageItemListener, RewardedVideoAdListener {

    private static final String TAG = "MainActivity";

    //UI
    private RecyclerView mRecyclerView;
    private RelativeLayout mLoadingScreen;

    //Vars
    private RewardedVideoAd mRewardedVideoAd;
    private ListAdapter mAdapter;

    private MainActivityPresenter mPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle(R.string.candidate);

        mPresenter = new MainActivityPresenter(this);
        mPresenter.bindView(this);

        initUI();
        initMobileAds();

        mPresenter.onViewCreated();
    }



    private void initUI() {
        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mAdapter = new ListAdapter(this);
        mRecyclerView.setAdapter(mAdapter);

        mLoadingScreen = findViewById(R.id.loading_screen);
    }


    private void initMobileAds() {
        MobileAds.initialize(this, getResources().getString(R.string.ads_identifier));
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);
    }

    /* =============================================
    * MainActivityView implementation
    * */

    @Override
    public void loadRewardedVideoAd(String videoId) {
        mRewardedVideoAd.loadAd(videoId, new AdRequest.Builder().build());
    }

    @Override
    public void showVideo() {
        if (mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.show();
        }
    }

    @Override
    public void showLoading() {
        mLoadingScreen.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingScreen.setVisibility(View.GONE);
    }

    @Override
    public void showMessageList(ArrayList<MessageRow> messages) {
        mAdapter.setData(messages);
    }

    @Override
    public void showEmptyList() {
        mAdapter.setData(new ArrayList<MessageRow>());
    }

    /* =============================================
     * OnMessageItemListener implementation
     * */
    @Override
    public void onSelfMessageClicked() {
        mPresenter.onSelfMessageClicked();
    }

    @Override
    public void onRewardedLinkPressed(String videoId) {
        mPresenter.onRewardLinkPressed(videoId);
    }


    /* =============================================
     * RewardedVideoAdListener implementation
     * */

    @Override
    public void onRewardedVideoAdLoaded() {
        Log.i(TAG, "onRewardedVideoAdLoaded: called");
        mPresenter.onRewardedVideoAdLoaded();
    }

    @Override
    public void onRewardedVideoAdOpened() {
        Log.i(TAG, "onRewardedVideoAdOpened: called");
    }

    @Override
    public void onRewardedVideoStarted() {
        Log.i(TAG, "onRewardedVideoStarted: called");
        mPresenter.onRewardedVideoStarted();
    }

    @Override
    public void onRewardedVideoAdClosed() {
        Log.i(TAG, "onRewardedVideoAdClosed: called");
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        Log.i(TAG, "onRewarded: called" + rewardItem);
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        Log.i(TAG, "onRewardedVideoAdLeftApplication: called");
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        Log.i(TAG, "onRewardedVideoAdFailedToLoad: called " + i);
        mPresenter.onRewardedVideoAdFailedToLoad();
    }

    @Override
    public void onRewardedVideoCompleted() {
        Log.i(TAG, "onRewardedVideoCompleted: called");
        mPresenter.onRewardedVideoCompleted();
    }
}

package com.novasolutio.android.recyclerviewtest.message_view;

public class MessageSelf implements MessageRow {

    private int id;
    private String mText;
    private String mDate;

    public MessageSelf(int id, String text, String date) {
        this.id = id;
        mText = text;
        mDate = date;
    }

    @Override
    public int getMessageType() {
        return MessageRow.MESSAGE_SELF;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }
}

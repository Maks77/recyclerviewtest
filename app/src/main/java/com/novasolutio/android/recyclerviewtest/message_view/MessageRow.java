package com.novasolutio.android.recyclerviewtest.message_view;

public interface MessageRow {

    int MESSAGE_SELF = 0;
    int MESSAGE_FOREIGN = 1;
    int MESSAGE_SPONSORED = 2;

    int getMessageType();
}

package com.novasolutio.android.recyclerviewtest.repository;

import com.novasolutio.android.recyclerviewtest.entities.Message;
import com.novasolutio.android.recyclerviewtest.message_view.MessageRow;

import java.util.List;

public interface IRepository {

    List<MessageRow> getAllMessages();

}

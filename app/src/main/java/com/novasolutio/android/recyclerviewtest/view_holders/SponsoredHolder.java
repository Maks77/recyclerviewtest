package com.novasolutio.android.recyclerviewtest.view_holders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.novasolutio.android.recyclerviewtest.R;
import com.novasolutio.android.recyclerviewtest.adapters.ListAdapter;
import com.novasolutio.android.recyclerviewtest.message_view.MessageRow;
import com.novasolutio.android.recyclerviewtest.message_view.MessageSponsored;

public class SponsoredHolder extends RecyclerView.ViewHolder {

    private TextView mLink;
    private ListAdapter.OnMessageItemListener mListener;
    private MessageSponsored mMessage;


    public void setListener(ListAdapter.OnMessageItemListener listener) {
        mListener = listener;
    }

    public SponsoredHolder(@NonNull View itemView) {
        super(itemView);

        mLink = itemView.findViewById(R.id.tv_link);
        mLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onRewardedLinkPressed(mMessage.getLinkId());
            }
        });
    }

    public void bind (MessageRow message) {
        mMessage = (MessageSponsored) message;

        mLink.setText(mMessage.getLink());
    }


}

package com.novasolutio.android.recyclerviewtest.view_holders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.novasolutio.android.recyclerviewtest.R;
import com.novasolutio.android.recyclerviewtest.message_view.MessageForeign;
import com.novasolutio.android.recyclerviewtest.message_view.MessageRow;

public class ForeignHolder extends RecyclerView.ViewHolder {

    private TextView mText, mDate;

    public ForeignHolder(@NonNull View itemView) {
        super(itemView);

        mText = itemView.findViewById(R.id.tv_foreign_text);
        mDate = itemView.findViewById(R.id.tv_foreign_date);
    }

    public void bind (MessageRow message) {
        MessageForeign mMessage = (MessageForeign) message;

        mText.setText(mMessage.getText());
        mDate.setText(mMessage.getDate());
    }
}

package com.novasolutio.android.recyclerviewtest.entities;

public class Message {

    private int id;
    private String text;
    private long date;
    private int type;

    public Message(int id, int type, String text, long date) {
        this.id = id;
        this.type = type;
        this.text = text;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}

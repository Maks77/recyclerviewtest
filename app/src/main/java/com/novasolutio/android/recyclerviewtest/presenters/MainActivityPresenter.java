package com.novasolutio.android.recyclerviewtest.presenters;

import android.content.Context;
import android.widget.Toast;

import com.novasolutio.android.recyclerviewtest.message_view.MessageRow;
import com.novasolutio.android.recyclerviewtest.repository.IRepository;
import com.novasolutio.android.recyclerviewtest.repository.Repository;
import com.novasolutio.android.recyclerviewtest.views.MainActivityView;

import java.util.ArrayList;

public class MainActivityPresenter {

    private static final String TAG = "MainActivityPresenter";

    //Vars
    private Context mContext;
    private ArrayList<MessageRow> mMessages;
    private IRepository mRepository;

    //View
    private MainActivityView mView;

    public MainActivityPresenter(Context context) {
        mContext = context;
        mRepository = Repository.getInstance(mContext);
        mMessages = ((Repository) mRepository).getAllMessages();
    }

    public void bindView(MainActivityView view) {
        mView = view;
    }

    public void onViewCreated() {
        if (!mMessages.isEmpty()) {
            mView.showMessageList(mMessages);
        } else {
            mView.showEmptyList();
        }
    }

    public void onSelfMessageClicked() {
        Toast.makeText(mContext, "CALLBACK TO ACTIVITY", Toast.LENGTH_SHORT).show();
    }

    public void onRewardLinkPressed(String videoId) {
        mView.showLoading();
        mView.loadRewardedVideoAd(videoId);
    }

    public void onRewardedVideoAdLoaded() {
        mView.showVideo();
    }

    public void onRewardedVideoStarted() {
        mView.hideLoading();
    }

    public void onRewardedVideoAdFailedToLoad() {
        Toast.makeText(mContext, "Failed to load video", Toast.LENGTH_SHORT).show();
        mView.hideLoading();
    }

    public void onRewardedVideoCompleted() {
        mView.hideLoading();
    }
}

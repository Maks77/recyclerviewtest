package com.novasolutio.android.recyclerviewtest.view_holders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.novasolutio.android.recyclerviewtest.R;
import com.novasolutio.android.recyclerviewtest.adapters.ListAdapter;
import com.novasolutio.android.recyclerviewtest.message_view.MessageRow;
import com.novasolutio.android.recyclerviewtest.message_view.MessageSelf;

public class SelfHolder extends RecyclerView.ViewHolder {

    private TextView mText, mDate;
    private ListAdapter.OnMessageItemListener mListener;

    public void setListener(ListAdapter.OnMessageItemListener listener) {
        mListener = listener;
    }

    public SelfHolder(@NonNull View itemView) {
        super(itemView);

        mText = itemView.findViewById(R.id.tv_self_text);
        mDate = itemView.findViewById(R.id.tv_self_date);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSelfMessageClicked();
            }
        });
    }

    public void bind(MessageRow message) {
        MessageSelf mMessage = (MessageSelf) message;

        mText.setText(mMessage.getText());
        mDate.setText(mMessage.getDate());
    }
}

package com.novasolutio.android.recyclerviewtest.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.novasolutio.android.recyclerviewtest.R;
import com.novasolutio.android.recyclerviewtest.message_view.MessageForeign;
import com.novasolutio.android.recyclerviewtest.message_view.MessageRow;
import com.novasolutio.android.recyclerviewtest.message_view.MessageSelf;
import com.novasolutio.android.recyclerviewtest.message_view.MessageSponsored;
import com.novasolutio.android.recyclerviewtest.view_holders.ForeignHolder;
import com.novasolutio.android.recyclerviewtest.view_holders.SelfHolder;
import com.novasolutio.android.recyclerviewtest.view_holders.SponsoredHolder;

import java.util.ArrayList;

public class ListAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private ArrayList<MessageRow> mMessages;
    private OnMessageItemListener mListener;


    public ListAdapter(Context context) {
        mContext = context;
        mMessages = new ArrayList<>();

        mListener = (OnMessageItemListener) context;
    }

    public void setData(ArrayList<MessageRow> messages) {
        mMessages = messages;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {

        MessageRow item = mMessages.get(position);

        if (item instanceof MessageSelf) {
            return MessageRow.MESSAGE_SELF;
        } else if (item instanceof MessageForeign) {
            return MessageRow.MESSAGE_FOREIGN;
        } else if (item instanceof MessageSponsored) {
            return MessageRow.MESSAGE_SPONSORED;
        } else {
            return -1;
        }
    }

    private MessageRow getMessageItem(int position) {
        return mMessages.get(position);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case MessageRow.MESSAGE_SELF:
                View viewSelf = LayoutInflater.from(mContext).inflate(R.layout.message_self, viewGroup, false);
                SelfHolder mSelfHolder = new SelfHolder(viewSelf);
                mSelfHolder.setListener(mListener);
                return mSelfHolder;

            case MessageRow.MESSAGE_FOREIGN:
                View viewForeign = LayoutInflater.from(mContext).inflate(R.layout.message_foreign, viewGroup, false);
                return new ForeignHolder(viewForeign);

            case MessageRow.MESSAGE_SPONSORED:
                View viewSponsored = LayoutInflater.from(mContext).inflate(R.layout.message_sponsored, viewGroup, false);
                SponsoredHolder sponsoredHolder = new SponsoredHolder(viewSponsored);
                sponsoredHolder.setListener(mListener);
                return sponsoredHolder;

            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        switch (getItemViewType(i)) {
            case MessageRow.MESSAGE_SELF:
                ((SelfHolder) viewHolder).bind(getMessageItem(i));
                break;
            case MessageRow.MESSAGE_FOREIGN:
                ((ForeignHolder) viewHolder).bind(getMessageItem(i));
                break;

            case MessageRow.MESSAGE_SPONSORED:
                ((SponsoredHolder) viewHolder).bind(getMessageItem(i));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    public interface OnMessageItemListener {
        void onSelfMessageClicked();
        void onRewardedLinkPressed(String videoId);
    }
}

package com.novasolutio.android.recyclerviewtest.message_view;

public class MessageForeign implements MessageRow {

    private int id;
    private String mText;
    private String mDate;

    public MessageForeign(int id, String text, String date) {
        this.id = id;
        mText = text;
        mDate = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    @Override
    public int getMessageType() {
        return MessageRow.MESSAGE_FOREIGN;
    }
}

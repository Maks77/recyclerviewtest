package com.novasolutio.android.recyclerviewtest.repository;

import android.content.Context;

import com.novasolutio.android.recyclerviewtest.R;
import com.novasolutio.android.recyclerviewtest.message_view.MessageForeign;
import com.novasolutio.android.recyclerviewtest.message_view.MessageRow;
import com.novasolutio.android.recyclerviewtest.message_view.MessageSelf;
import com.novasolutio.android.recyclerviewtest.message_view.MessageSponsored;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Repository implements IRepository {

    private static Repository instance;
    private ArrayList<MessageRow> mMessages;
    private DateFormat mDateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private Context mContext;

    public static Repository getInstance(Context context) {
        if (instance == null) {
            instance = new Repository(context);
        }
        return instance;
    }

    public Repository(Context context) {
        mContext = context;
        mMessages = new ArrayList<>();

        mockMessages();
    }

    @Override
    public ArrayList<MessageRow> getAllMessages() {
        return mMessages;
    }

    private void mockMessages() {
        mMessages.add(new MessageForeign(0, "Hi there! I salut you in this sample app", mDateFormat.format(new Date())));
        mMessages.add(new MessageSelf(1, "Hi", mDateFormat.format(new Date())));
        mMessages.add(new MessageForeign(2, "You can see integrated gms sample testing video reward add below!", mDateFormat.format(new Date())));
        mMessages.add(new MessageSponsored(3, "Show reward", mContext.getResources().getString(R.string.ads_test_revarded_video)));
        mMessages.add(new MessageSelf(4, "Thank you", mDateFormat.format(new Date())));
        mMessages.add(new MessageForeign(5, "Here is another one:", mDateFormat.format(new Date())));
        mMessages.add(new MessageSponsored(6, "Show another reward", mContext.getResources().getString(R.string.ads_test_revarded_video)));
    }
}

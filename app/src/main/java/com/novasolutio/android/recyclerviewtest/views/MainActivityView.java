package com.novasolutio.android.recyclerviewtest.views;

import com.novasolutio.android.recyclerviewtest.message_view.MessageRow;

import java.util.ArrayList;

public interface MainActivityView {

    void showMessageList(ArrayList<MessageRow> messages);
    void showEmptyList();
    void showLoading();
    void hideLoading();
    void loadRewardedVideoAd(String videoId);
    void showVideo();

}
